import 'dart:async';

import 'package:flutter/material.dart';
import 'package:suit_app/utils/color_hex.dart';

class splashScreen extends StatefulWidget {
  splashScreen({Key key}) : super(key: key);

  _splashScreenState createState() => _splashScreenState();
}

class _splashScreenState extends State<splashScreen> {

  @override
  void initState() { 
    super.initState();
    Timer(Duration(seconds : 1), () => print("screen Done"));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _Screen(),
    );
  }

  _Screen() {
    return Stack(fit: StackFit.expand, children: <Widget>[
      Container(
        decoration: BoxDecoration(color: HexColor("#108eeb")),
      ),
    ]);
  }
}
